<table>
    <thead>
    <tr>
        <th>Имя</th>
        <th>Гос. номер</th>
        <th>Vin номер</th>
        <th>Цвет</th>
        <th>Марка</th>
        <th>Модель</th>
        <th>Год</th>
    </tr>
    </thead>
    <tbody>
    @foreach($cars as $car)
        <tr>
            <td>{{ $car->name }}</td>
            <td>{{ $car->license_plate }}</td>
            <td>{{ $car->vin }}</td>
            <td>{{ $car->color }}</td>
            <td>{{ $car->brand }}</td>
            <td>{{ $car->model }}</td>
            <td>{{ $car->model_year }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
