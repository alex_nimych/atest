<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StolenCars extends Model
{
    protected $fillable = [
        'name', 'license_plate', 'color', 'vin', 'brand', 'model_year', 'model'
    ];

    public static function getFilter($filter = null, $offset = 0, $limit = 10, $sort = 'created_at', $type = 'ASC'){
        $result = DB::table('stolen_cars');
        if ($filter and count($filter)>0){
            foreach ($filter as $key => $val){
                $result->where($key, '=', $val);
            }
        }


        $result->offset($offset);
        $result->limit($limit);
        $result->orderBy($sort,$type);
        return $result->get();
    }
    public function scopeFilter(Builder $builder, QueryFilter $filters)
    {
        return $filters->apply($builder);
    }
    //
}
