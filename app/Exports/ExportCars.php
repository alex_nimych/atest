<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExportCars implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public $cars;

    public function __construct($cars)
    {
        $this->cars = $cars;
    }

    public function view(): View
    {
        return view('cars', [
            'cars' => $this->cars
        ]);
    }
}
