<?php

namespace App\Http\Controllers\Api;

use App\Exports\ExportCars;
use App\Http\Requests\StoreCar;
use App\Http\Controllers\Api\ApiController;
use App\StolenCars;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Storage;

class CarsController extends ApiController
{
    public $page = 1;
    public $offset = 1;
    public $limit = 10;
    public $sort = 'id';
    public $type = 'ASC';
    public $generateFile = false;
    public $download = false;
    public $download_url = null;
    public $request;
    public $filters;

    public function __construct(Request $request)
    {
        $this->filters = $_GET;
        if (isset($_GET['page'])){
            $this->page = $_GET['page'];
            unset($this->filters['page']);
        }
        if (isset($_GET['limit'])){
            $this->limit = $_GET['limit'];
            unset($this->filters['limit']);
        }
        $this->offset = $this->limit*($this->page-1);
        if (isset($_GET['sort'])){
            $this->sort = $_GET['sort'];
            unset($this->filters['sort']);
        }
        if (isset($_GET['type'])){
            $this->type = $_GET['type'];
            unset($this->filters['type']);
        }
        if (isset($_GET['generate_file'])){
            $this->generateFile = $_GET['generate_file'];
            unset($this->filters['generate_file']);
        }
        if (isset($_GET['download_url'])){
            $this->generateFile = $_GET['download_url'];
            unset($this->filters['download_url']);
        }
        if (isset($_GET['download'])){
            $this->download = $_GET['download'];
            unset($this->filters['download']);
        }

    }

    public function index(){
        $cars = StolenCars::getFilter($this->filters, $this->offset, $this->limit, $this->sort, $this->type);
        if ($this->generateFile){
            if ($this->download){
                return Excel::download(new ExportCars($cars),'test.xlsx');
            }
            $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] .'&download=true' ;
        }else{
            $url = null;
        }
        return $this->sendResponse(['download_url'=>$url, 'cars'=>$cars->toArray()], 'Cars retrieved successfully.');
    }
    public function show($id){
        $car = StolenCars::find($id);
        if ($car){
            return $this->sendResponse($car->toArray(), 'Cars retrieved successfully.');
        }else{
            return $this->sendMessage('Cars no found.');
        }
    }
    public function store(StoreCar $request){
        $car = new StolenCars();
        $request->validated();
        $data = $request->all();
        $response =file_get_contents('https://vpic.nhtsa.dot.gov/api/vehicles/decodevinvalues/'.$data['vin'].'?format=json&');
        $response  = json_decode($response, true);
        $data['brand'] = $response['Results'][0]['Make'];
        $data['model'] = $response['Results'][0]['Model'];
        $data['model_year'] = $response['Results'][0]['ModelYear'];
        $car->fill($data);
        $car->save();
        return $this->sendResponse($car, 'New car true.');

    }
    public function update(StoreCar $request, $id){
        $car = StolenCars::find($id);
        if ($car){
            $data = $request->all();
            $response =file_get_contents('https://vpic.nhtsa.dot.gov/api/vehicles/decodevinvalues/'.$data['vin'].'?format=json&');
            $response  = json_decode($response, true);
            $data['brand'] = $response['Results'][0]['Make'];
            $data['model'] = $response['Results'][0]['Model'];
            $data['model_year'] = $response['Results'][0]['ModelYear'];
            $car->fill($data);
            $car->save();
            return $this->sendResponse($car->toArray(), 'Car updated.');
        }else{
            return $this->sendMessage('Car no found.');
        }
    }
    public function destroy($id){
        $car = StolenCars::find($id);
        if ($car){
        $car->delete();
            return $this->sendResponse([], 'Car deleted.');
        }else{
            return $this->sendMessage('Car deleted.');
        }

    }
    public function export(){
        $cars = StolenCars::getFilter($this->filters, $this->offset, $this->limit, $this->sort, $this->type);
        if ($this->generateFile){
            $name = '/public/export/Export'. time(). 'xlsx';
            Storage::put($name,Excel::download(new ExportCars($cars),'test.xlsx'));
        }
    }

}
