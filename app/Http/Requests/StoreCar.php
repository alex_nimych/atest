<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCar extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required|max:255',
            'license_plate'=> 'required|max:255|unique:stolen_cars',
            'color'=> 'required|max:255',
            'vin'=> 'required|max:17|min:17|unique:stolen_cars',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Необходимо заполнить name не более 255 символов.',
            'name.max' => 'Необходимо заполнить name не более 255 символов.',
            'license_plate.required'  => 'Необходимо аполнить гос номер авто!',
            'license_plate.unique'  => 'Авто с таким номером уже есть в базе!',
            'color.required'  => 'Необходимо указать цвет авто!',
            'color.max'  => 'color не может быть более 50 символов.',
            'vin.required'  => 'Заполните vin',
            'vin.min'  => 'Vin номер должен быть должен состоять из 17 символов!',
            'vin.max'  => 'Vin номер должен быть должен состоять из 17 символов!',
            'vin.unique'  => 'Такой Vin номер уже есть в базе!',
        ];
    }
}
